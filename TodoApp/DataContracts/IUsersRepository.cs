using System.Collections.Generic;
using TodoApp.Data.Entities;
using TodoApp.DataContracts.Requests;

namespace TodoApp.Data
{
    public interface IUsersRepository
    {
        List<User> GetAll(int? from, int? to);
        User GetById(int id);
        User Create(CreateUserRequest request);
        void Update(int id, UpdateUserRequest request);
        void Delete(int id);
    }
}