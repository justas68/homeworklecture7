﻿namespace TodoApp.DataContracts.Enums
{
    public enum SortDirection
    {
        Asc,

        Desc
    }
}
