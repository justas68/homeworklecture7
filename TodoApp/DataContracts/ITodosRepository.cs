﻿using System;
using System.Collections.Generic;
using TodoApp.Data.Entities;
using TodoApp.DataContracts.Requests;

namespace TodoApp.DataContracts
{
    public interface ITodosRepository
    {
        List<TodoItem> GetAll(GetAllTodosRequest request, int? page);

        TodoItem GetById(int id);

        TodoItem Create(CreateTodoItemRequest request);

        void Update(int id, UpdateTodoItemRequest request);

        void Delete(int id);
    }
}