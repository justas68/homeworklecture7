using System.ComponentModel.DataAnnotations;

namespace TodoApp.DataContracts.Requests
{
    public class UpdateUserRequest
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public int Age{ get; set; }

        [Required]
        public bool IsAdmin { get; set; }

    }
}
