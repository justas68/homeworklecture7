namespace TodoApp.Data.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public bool IsAdmin { get; set; }
    }
}