using System;
using System.Collections.Generic;
using System.Linq;
using TodoApp.Data.Entities;
using TodoApp.DataContracts;
using TodoApp.DataContracts.Enums;
using TodoApp.DataContracts.Requests;

namespace TodoApp.Data
{
  public class UsersRepository : IUsersRepository
  {
    private readonly AppDbContext _dbContext;
    public UsersRepository(AppDbContext appDbContext)
    {
      _dbContext = appDbContext;
    }

    public List<User> GetAll(int? from, int? to)
    {
      var query = _dbContext.Users.AsQueryable();
        if (from == null)
        {
            from = 0;
        }
        if (to == null)
        {
            to = 150;
        }

        query = query.Where(x => x.Age > from && x.Age < to);
            return query.ToList();
    }

    public User GetById(int id)
    {
      return _dbContext.Users.Single(x => x.Id == id);
    }

    public User Create(CreateUserRequest request)
    {
      var user = new User
      {
        Name= request.Name,
        IsAdmin = request.IsAdmin,
        Age = request.Age
      };

      _dbContext.Users.Add(user);

      _dbContext.SaveChanges();

      return user;
    }

    public void Update(int id, UpdateUserRequest request)
    {
      var user = _dbContext.Users.Single(x => x.Id == id);

      user.Name = request.Name;
      user.IsAdmin = request.IsAdmin;
      user.Age = request.Age;

      _dbContext.SaveChanges();
    }

    public void Delete(int id)
    {
      var user = _dbContext.Users.Single(x => x.Id == id);

      _dbContext.Users.Remove(user);

      _dbContext.SaveChanges();
    }
  }
}
