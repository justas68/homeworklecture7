﻿using System;
using System.Collections.Generic;
using System.Linq;
using TodoApp.Data.Entities;
using TodoApp.DataContracts;
using TodoApp.DataContracts.Enums;
using TodoApp.DataContracts.Requests;

namespace TodoApp.Data
{
  public class TodosRepository : ITodosRepository
  {
    private readonly AppDbContext _dbContext;
    public TodosRepository(AppDbContext appDbContext)
    {
      _dbContext = appDbContext;
    }

    public List<TodoItem> GetAll(GetAllTodosRequest request, int? page)
    {
      var query = _dbContext.TodoItems.AsQueryable();
      if (!string.IsNullOrEmpty(request.Query))
      {
        query = query.Where(x => x.Title.ToLowerInvariant().Contains(request.Query.ToLowerInvariant()));
      }

      if (request.Completed.HasValue)
      {
        query = query.Where(x => x.Completed == request.Completed.Value);
      }

      if (request.SortDirection.HasValue)
      {
        query = request.SortDirection.Value == SortDirection.Asc
            ? query.OrderBy(x => x.Title)
            : query.OrderByDescending(x => x.Title);
      }
      var list = query.ToList();
      if (page != null)
      {
        if ((page - 1) * 3 > list.Count)
        {
          list.Clear();
        }
        else
        {
            list = list.Skip((int)(page-1) * 3).Take(3).ToList();
        }
      }
      return list;
    }

    public TodoItem GetById(int id)
    {
      return _dbContext.TodoItems.Single(x => x.Id == id);
    }

    public TodoItem Create(CreateTodoItemRequest request)
    {
      if (_dbContext.TodoItems.FirstOrDefault(x => x.Title.ToLowerInvariant() == request.Title.ToLowerInvariant()) != null)
      {
        throw new ArgumentException($@"Todo item with title ""{request.Title}"" already exists.");
      }

      var item = new TodoItem
      {
        Title = request.Title,
        Completed = false
      };

      _dbContext.TodoItems.Add(item);

      _dbContext.SaveChanges();

      return item;
    }

    public void Update(int id, UpdateTodoItemRequest request)
    {
      var item = _dbContext.TodoItems.Single(x => x.Id == id);

      item.Title = request.Title;
      item.Completed = request.Completed.GetValueOrDefault();

      _dbContext.SaveChanges();
    }

    public void Delete(int id)
    {
      var item = _dbContext.TodoItems.Single(x => x.Id == id);

      _dbContext.TodoItems.Remove(item);

      _dbContext.SaveChanges();
    }
  }
}
