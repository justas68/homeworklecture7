﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TodoApp.Data;
using TodoApp.Data.Entities;
using TodoApp.DataContracts.Requests;
using TodoApp.DataContracts.Responses;

namespace TodoApp.Controllers
{
  [Produces("application/json")]
  [Route("api/users")]
  public class UsersController : Controller
  {
    private readonly IUsersRepository _usersRepository;
    public UsersController(IUsersRepository usersRepository)
    {
      this._usersRepository = usersRepository;
    }
    [HttpGet]
    public List<User> GetAll([FromQuery]int? from, [FromQuery]int? to)
    {
      return _usersRepository.GetAll(from, to);
    }

    [HttpGet("{id}")]
    public IActionResult GetById(int id)
    {
      try
      {
        return Ok(_usersRepository.GetById(id));
      }
      catch (InvalidOperationException)
      {
        return NotFound();
      }
    }

    [HttpPost]
    public IActionResult CreateUser([FromBody]CreateUserRequest request)
    {
      if (ModelState.IsValid)
      {
        try
        {
          var item = _usersRepository.Create(request);

          return CreatedAtAction(nameof(GetById), item);
        }
        catch (ArgumentException e)
        {
          return StatusCode((int)HttpStatusCode.Conflict, new ErrorResponse { ErrorMessage = e.Message });
        }
      }
      else
      {
        return BadRequest(ModelState);
      }
    }

    [HttpPut("{id}")]
    public IActionResult UpdateUser(int id, [FromBody]UpdateUserRequest request)
    {
      if (ModelState.IsValid)
      {
        try
        {
          _usersRepository.Update(id, request);

          return NoContent();
        }
        catch (InvalidOperationException)
        {
          return NotFound();
        }
      }
      else
      {
        return BadRequest(ModelState);
      }
    }

    [HttpDelete("{id}")]
    public IActionResult DeleteUser(int id)
    {
      try
      {
        _usersRepository.Delete(id);

        return NoContent();
      }
      catch (InvalidOperationException)
      {
        return NotFound();
      }
    }
  }
}