﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using TodoApp.Data.Entities;
using TodoApp.DataContracts;
using TodoApp.DataContracts.Requests;
using TodoApp.DataContracts.Responses;

namespace TodoApp.Controllers
{
    [Route("api/todos")]
    public class TodosController : Controller
    {
        private readonly ITodosRepository todosRepository;

        public TodosController(ITodosRepository todosRepository)
        {
            this.todosRepository = todosRepository;
        }

        [HttpGet]
        public List<TodoItem> GetAll([FromQuery]GetAllTodosRequest request, [FromQuery]int? page)
        {
            return todosRepository.GetAll(request, page);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                return Ok(todosRepository.GetById(id));
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public IActionResult CreateItem([FromBody]CreateTodoItemRequest request)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    var item = todosRepository.Create(request);

                    return CreatedAtAction(nameof(GetById), item);
                }
                catch(ArgumentException e)
                {
                    return StatusCode((int)HttpStatusCode.Conflict, new ErrorResponse { ErrorMessage = e.Message });
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPut("{id}")]
        public IActionResult UpdateItem(int id, [FromBody]UpdateTodoItemRequest request) {
            if(ModelState.IsValid)
            {
                try
                {
                    todosRepository.Update(id, request);

                    return NoContent();
                }
                catch(InvalidOperationException)
                {
                    return NotFound();
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteItem(int id)
        {
            try
            {
                todosRepository.Delete(id);

                return NoContent();
            }
            catch(InvalidOperationException)
            {
                return NotFound();
            }
        }
    }
}
