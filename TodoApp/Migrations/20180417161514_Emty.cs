﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TodoApp.Migrations
{
  public partial class Emty : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql("INSERT INTO [dbo].[TodoItems] ([Completed], [Title]) VALUES(0, 'Inserted through migration')");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql("DELETE FROM [dbo].[TodoItems] WHERE Title = 'Inserted through migration' AND Completed = 0");
    }
  }
}
