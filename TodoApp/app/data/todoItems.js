export default [
    {
        id: 0,
        title: 'Walk the dog',
        completed: false
    },
    {
        id: 1,
        title: 'Take out the trash',
        completed: true
    },
    {
        id: 2,
        title: 'Vacuum the carpet',
        completed: true
    },
    {
        id: 3,
        title: 'Take out a person',
        completed: false
    }
];